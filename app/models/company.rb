class Company < ApplicationRecord

  scope :multiple, lambda { |ids_list| where("id in (?)", ids_list.is_a?(String) ? ids_list.split(',') : [*ids_list]) }

  belongs_to :user, optional: true
  has_many :contacts
  has_many :items
  has_many :taxes  
  has_many :invoices
  has_many :expenses



  mount_uploader :companylogo, CompanylogoUploader
  acts_as_archival
  acts_as_paranoid

  def self.filter(params)
    mappings = {active: 'unarchived', archived: 'archived', deleted: 'only_deleted'}
    method = mappings[params[:status].to_sym]
    params[:account].companies.send(method).page(params[:page]).per(params[:per])
  end

  def self.recover_archived(ids)
    multiple(ids).map(&:unarchive)
  end

  def self.recover_deleted(ids)
    multiple(ids).only_deleted.each { |company| company.restore; company.unarchive }
  end


end
