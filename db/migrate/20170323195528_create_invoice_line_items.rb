class CreateInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_line_items do |t|
      t.string :item_name
      t.string :item_description
      t.decimal :item_unit_cost, precision: 10, scale: 2
      t.decimal :itam_quantity, precision: 10, scale: 2
      t.decimal :gross_line_total, precision: 10, scale: 2
      t.decimal :discount_amount, precision: 10, scale: 2
      t.integer :tax_1
      t.integer :tax_2

      t.timestamps
    end
  end
end
