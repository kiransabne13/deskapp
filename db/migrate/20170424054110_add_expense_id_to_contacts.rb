class AddExpenseIdToContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :expense_id, :integer
  end
end
