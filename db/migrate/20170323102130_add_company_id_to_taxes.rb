class AddCompanyIdToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :company_id, :integer
  end
end
