class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :item_name
      t.string :item_description
      t.decimal :item_unit_cost, precision: 10, scale: 2
      t.decimal :item_quantity, precision: 10, scale: 2
      t.integer :tax_1
      t.integer :tax_2
      t.decimal :item_actual_price, precision: 10, scale: 2

      t.timestamps
    end
  end
end
