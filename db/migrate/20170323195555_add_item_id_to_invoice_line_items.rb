class AddItemIdToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :item_id, :integer
  end
end
