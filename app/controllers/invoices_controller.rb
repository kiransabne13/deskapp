class InvoicesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]
  include InvoicesHelper

  # GET /invoices
  # GET /invoices.json
  def index
    @invoices = current_user.current_company.invoices
  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
    @invoice = current_user.current_company.invoices.find(params[:id])
    @contact = Contact.unscoped.find_by_id @invoice.contact_id
    respond_to do |format|
      format.html
    end
  end

  def invoice_pdf
    @images_path = "#{request.protocol}#{request.host_with_port}/assets"
    @invoice = Invoice.find(params[:id])
    @contact = Contact.unscoped.find_by_id @invoice.contact_id
    respond_to do |format|
      format.pdf do
	file_name = "Invoice-#{Date.today.to_s}.pdf"
 	pdf = render_to_string  pdf: "#{@invoice.invoice_number}",
	  layout: 'pdf_mode.html.erb',
	  encoding: "UTF-8",
	  template: 'invoices/invoice_pdf.html.erb',
	  footer:{
	    right: 'Page [page] of [topage]'
	  }
	send_data pdf, filename: file_name
    end
   end
  end

  # GET /invoices/new
  def new
    @invoice = current_user.current_company.invoices.new
    @contact = Contact.find params[:invoice_for_contact] if params[:invoice_for_contact].present?
    @contact = @invoice.contact if params[:id].present?
    get_contacts_and_items
    
    respond_to do |format|
      format.html # new.html.erb
      format.js
      #format.json { render :json => @invoice }
    end
  end

  # GET /invoices/1/edit
  def edit
    @invoice = Invoice.find(params[:id])
    @invoice.invoice_line_items.build()
    get_contacts_and_items
  end

  # POST /invoices
  # POST /invoices.json
  def create
    @invoice = current_user.current_company.invoices.new(invoice_params)
    @invoice.create_line_item_taxes()
    respond_to do |format|
      if @invoice.save
        format.html { redirect_to @invoice, notice: 'Invoice was successfully created.' }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html { render :new }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoices/1
  # PATCH/PUT /invoices/1.json
  def update
    @invoice = current_user.current_company.invoices.find(params[:id])
#    @invoice.company_id = get_company_id()
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to @invoice, notice: 'Invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice = current_user.current_company.invoices.find(params[:id])
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to invoices_url, notice: 'Invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:invoice_number, :invoice_date, :terms, :notes, :status, :subtotal, :invoice_total, :invoice_line_items_attributes, :archive_number, :archived_at, :deleted_at, :shippingcharges, :company_id, :due_date, :contact_id, invoice_line_items_attributes: [:id, :invoice_id, :item_id, :item_name, :item_description, :item_unit_cost, :item_quantity, :gross_line_total, :discount_amount, :tax_1, :tax_2, :line_total, :taxamountone, :taxtypeone, :taxrateone, :taxamounttwo, :taxratetwo, :taxtypetwo, :subtotal, :invoicetotal, :net_total, :_destroy])
    end
end
