class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :company_name
      t.string :company_contact_name
      t.string :contact_title
      t.string :company_country
      t.string :company_city
      t.string :company_street_address_1
      t.string :company_street_address_2
      t.string :company_state
      t.string :company_zipcode
      t.string :company_phone_number
      t.string :company_email
      t.string :company_cin
      t.string :company_vat

      t.timestamps
    end
  end
end
