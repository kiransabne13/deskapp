class AddArchiveNumberToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :archive_number, :string
  end
end
