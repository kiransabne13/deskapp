class AddUserIdToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :user_id, :integer
  end
end
