class AddArchiveNumberToContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :archive_number, :string
  end
end
