json.extract! contact, :id, :contact_company_name, :contact_first_name, :contact_last_name, :contact_email, :contact_company_phone, :created_at, :updated_at
json.url contact_url(contact, format: :json)
