json.extract! expense, :id, :expense_amount, :expense_date, :expense_note, :taxtypeone, :taxrateone, :taxamountone, :taxtypetwo, :taxratetwo, :taxamounttwo, :expense_total, :created_at, :updated_at
json.url expense_url(expense, format: :json)
