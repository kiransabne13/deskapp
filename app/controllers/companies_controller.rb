class CompaniesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  include CompaniesHelper
  # GET /companies
  # GET /companies.json
  def index
    @companies = current_user.companies

    respond_to do |format|
      format.html
      format.json { render json: @companies }
      format.js
  end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @company = current_user.companies.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @company }
  end
  end

  # GET /companies/new
  def new
    @company = Company.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/1/edit
  def edit
    @company = current_user.companies.find(params[:id])
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = current_user.companies.build(company_params)

    respond_to do |format|
      if @company.save
	CompanyMailer.added_company(@company).deliver
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    @company = current_user.companies.find(params[:id])
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = current_user.companies.find(params[:id])
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def filter_companies
    @companies = Company.filter(params.merge(per: session["#{controller_name}-per_page"], user: current_user)).order(sort_column + " " + sort_direction)
    respond_to { |format| format.js }
  end

  def select
    session['current_company'] = params[:id]
    current_user.update_attributes(current_company: params[:id])
    company =  Company.find(params[:id])
    render :text => company.company_name
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:company_name, :company_contact_name, :contact_title, :company_country, :company_city, :company_street_address_1, :company_street_address_2, :company_state, :company_zipcode, :company_phone_number, :company_email, :company_cin, :company_vat, :bank_info, :companylogo, :company_tag_line, :companywebsite)
    end
end
