class ApplicationController < ActionController::Base
  include ApplicationHelper
  protect_from_forgery with: :exception



  def associate_entity(params, entity)
    ids, controller = params[:company_ids], params[:controller]

    ActiveRecord::Base.transaction do
      Company.multiple(ids).each { |company| company.send(controller) << entity } unless ids.blank?
    end
  end



  def filter_by_company(elem, tbl=params[:controller])
    # set company dropdown session and save in database if company is changed
    unless params[:company_id].blank?
      session['current_company'] = params[:company_id]
      current_user.update_attributes(current_company: params[:company_id])
    end
    elem.where("#{tbl}.company_id IN(?)", get_company_id())
  end

  helper_method :filter_by_company

  def new_selected_company_name
    session['current_company'] = params[:company_id]
    current_user.update_attributes(current_company: params[:company_id])
    company =  Company.find(params[:company_id])
    render :text => company.company_name
  end



  def get_company_id
   session['current_company'] || current_user.current_company || current_user.first_company_id
  end

  def get_contacts_and_items
    parent = Company.find(params[:company_id] || get_company_id)
    @get_contacts = get_contacts(parent)
    @get_items = get_items(parent)
    @parent_class = parent.class.to_s
  end


  # generate clients options associated with company
  def get_contacts(parent)
    options = ''
    parent.contacts.each { |contact| options += "<option value=#{contact.id} type='company_level'>#{contact.contact_company_name}</option>" } if parent.contacts.present?
    options
  end

  # generate items options associated with company
  def get_items(parent)
    options = ''
    parent.items.each { |item| options += "<option value=#{item.id} type='company_level'>#{item.item_name}</option>" } if parent.items.present?
    options
  end


  def set_date_format
    gon.dateformat = get_date_format
  end

  def set_company_session
   unless params[:company_id].blank?
    session['current_company'] = params[:company_id]
    current_user.update_attributes(current_company: params[:company_id])
   end
  end


  def set_current_user
   User.current = current_user
  end


end
