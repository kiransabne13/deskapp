class AddArchivedAtToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :archived_at, :datetime
  end
end
