class Invoice < ApplicationRecord

  scope :multiple, ->(ids_list) {where("id in (?)", ids_list.is_a?(String) ? ids_list.split(',') : [*ids_list]) }
  scope :current_invoices,->(company_id){ where("IFNULL(due_date, invoice_date) >= ?", Date.today).where(company_id: company_id).order('created_at DESC')}
  scope :past_invoices, -> (company_id){where("IFNULL(due_date, invoice_date) < ?", Date.today).where(company_id: company_id).order('created_at DESC')}


  belongs_to :contact, optional: true
  belongs_to :invoice, optional: true
  belongs_to :company, optional: true
  belongs_to :user, optional: true
  has_many :invoice_line_items, :dependent => :destroy
  accepts_nested_attributes_for :invoice_line_items, :reject_if => proc { |line_item| line_item["item_id"].blank? }, :allow_destroy => true

  before_create :set_invoice_number

  acts_as_archival
  acts_as_paranoid

  def set_invoice_number
    self.invoice_number = Invoice.get_next_invoice_number(nil)
  end

  def self.get_next_invoice_number user_id
    ((Invoice.with_deleted.maximum("id") || 0) + 1).to_s.rjust(5, "0")
  end

  def self.filter(params, per_page)
    mappings = {active: 'unarchived', archived: 'archived', deleted: 'only_deleted'}
    method = mappings[params[:status].to_sym]
    self.send(method).page(params[:page]).per(per_page)
  end

  def create_line_item_taxes
    self.invoice_line_items.each do |invoice_line_item|
      if invoice_line_item.tax_one.present? and invoice_line_item.tax_one != invoice_line_item.tax1.try(:tax_id)
        tax_1 = Tax.find invoice_line_item.tax_one
        invoice_line_item.tax1 = LineItemTax.create(name: tax_1.name, percentage: tax_1.percentage, tax_id: tax_1.id)
      end
      if invoice_line_item.tax_two.present? and invoice_line_item.tax_two != invoice_line_item.tax2.try(:tax_id)
        tax_2 = Tax.find invoice_line_item.tax_two
        invoice_line_item.tax2 = LineItemTax.create(name: tax_2.name, percentage: tax_2.percentage, tax_id: tax_2.id)
      end
    end
    self.save
  end

  def update_line_item_taxes
    self.invoice_line_items.each do |invoice_line_item|
      if invoice_line_item.tax_one.present? and invoice_line_item.tax_one != invoice_line_item.tax1.try(:tax_id)
        tax_1 = LineItemTax.find_by_id(invoice_line_item.tax_one).present? ? LineItemTax.find(invoice_line_item.tax_one)  : Tax.find(invoice_line_item.tax_one)
        invoice_line_item.tax1 = tax_1.class.to_s == 'Tax' ? LineItemTax.create(name: tax_1.name, percentage: tax_1.percentage, tax_id: tax_1.id) : tax_1
      end
      if invoice_line_item.tax_two.present? and invoice_line_item.tax_two != invoice_line_item.tax2.try(:tax_id)
        tax_2 = LineItemTax.find_by_id(invoice_line_item.tax_two).present? ? LineItemTax.find(invoice_line_item.tax_two)  : Tax.find(invoice_line_item.tax_two)
        invoice_line_item.tax2 = tax_2.class.to_s == 'Tax' ? LineItemTax.create(name: tax_2.name, percentage: tax_2.percentage, tax_id: tax_2.id) : tax_2
      end
    end
    self.save
  end

  def load_deleted_tax1(line_item)
    Tax.unscoped.find_by_id(line_item.tax_1)
  end

  def load_deleted_tax2(line_item)
    Tax.unscoped.find_by_id(line_item.tax_2)
  end

  def load_archived_tax1(line_item)
    Tax.find_by_id(line_item.tax_1)
  end

  def load_archived_tax2(line_item)
    Tax.find_by_id(line_item.tax_2)
  end

  def unscoped_contact
    Contact.unscoped.find_by_id self.contact_id
  end

end
