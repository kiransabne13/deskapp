class AddExpenseIdToExpenseCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :expense_categories, :expense_id, :integer
  end
end
