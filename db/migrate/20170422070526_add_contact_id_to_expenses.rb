class AddContactIdToExpenses < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :contact_id, :integer
  end
end
