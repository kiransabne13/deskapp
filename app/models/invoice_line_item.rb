class InvoiceLineItem < ApplicationRecord

  belongs_to :item, optional: true
  belongs_to :tax1, :foreign_key => 'tax_1', :class_name => 'Tax'
  belongs_to :tax2, :foreign_key => 'tax_2', :class_name => 'Tax'
  belongs_to :invoice, optional: true

  acts_as_archival
  acts_as_paranoid

  attr_accessor :tax_one, :tax_two

  def unscoped_item
    Item.unscoped.find_by_id self.item_id
  end

end
