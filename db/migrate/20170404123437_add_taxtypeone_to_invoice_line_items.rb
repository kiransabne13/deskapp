class AddTaxtypeoneToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :taxtypeone, :string
  end
end
