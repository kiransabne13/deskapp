class AddItemQuantityToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :item_quantity, :decimal, precision: 10, scale: 2
  end
end
