class AddContactIdToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :contact_id, :integer
  end
end
