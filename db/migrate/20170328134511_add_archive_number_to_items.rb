class AddArchiveNumberToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :archive_number, :string
  end
end
