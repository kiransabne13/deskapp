class AddInvoiceWordsToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :invoice_word, :string
  end
end
