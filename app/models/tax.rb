class Tax < ApplicationRecord

  scope :multiple, lambda { |ids_list| where("id in (?)", ids_list.is_a?(String) ? ids_list.split(',') : [*ids_list]) }
  scope :archive_multiple, lambda {|ids| multiple(ids).map(&:archive)}
  scope :delete_multiple, lambda {|ids| multiple(ids).map(&:destroy)}

  has_many :items
  belongs_to :user, optional: true
  belongs_to :company, optional: true
  has_many :invoice_line_items
  validates :name, :presence => true
  validates :percentage, :presence => true

  acts_as_archival
  acts_as_paranoid


  def self.is_exits? tax_name
    where(name: tax_name).present?
  end

  def self.recover_archived(ids)
    multiple(ids).each {|tax| tax.archive_number = nil; tax.archived_at = nil; tax.save}
  end

  def self.recover_deleted(ids)
    multiple(ids).only_deleted.each { |tax| tax.archive_number = nil; tax.archived_at = nil; tax.deleted_at = nil; tax.save }
  end

  def self.filter(params, per_page)
    mappings = {active: 'unarchived', archived: 'archived', deleted: 'only_deleted'}
    method = mappings[params[:status].to_sym]
    self.send(method).page(params[:page]).per(per_page)
  end

end
