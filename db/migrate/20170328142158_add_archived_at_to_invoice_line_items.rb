class AddArchivedAtToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :archived_at, :datetime
  end
end
