class AddCompanywebsiteToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :companywebsite, :string
  end
end
