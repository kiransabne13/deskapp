class Contact < ApplicationRecord

  scope :multiple, lambda { |ids| where('id IN(?)', ids.is_a?(String) ? ids.split(',') : [*ids]) }

  belongs_to :company, optional: true
  belongs_to :user, optional: true
  has_many :invoices
  has_many :expenses
  acts_as_archival
  acts_as_paranoid

  def contact_company_name
    self[:contact_company_name].blank? ? self.contact_name : self[:contact_company_name]
  end

  def contact_name
    "#{contact_first_name} #{contact_last_name}"
  end

  def last_invoice
    invoices.unarchived.first.id rescue nil
  end

  def self.archive_multiple ids
    multiple(ids).map(&:archive)
  end

  def self.delete_multiple ids
    multiple(ids).map(&:destroy)
  end

  def self.recover_archived ids
    multiple(ids).map(&:unarchive)
  end

  def self.recover_deleted ids
    multiple(ids).only_deleted.each {|client| client.restore; client.unarchive; client.client_contacts.only_deleted.map(&:restore);}
  end

  def self.filter(params)
    mappings = {active: 'unarchived', archived: 'archived', deleted: 'only_deleted'}
    method = mappings[params[:status].to_sym]
    self.send(method).page(params[:page]).per(params[:per])
  end

  def self.get_contacts(params)
    company = params[:user].current_company

    # get the clients associated with companies
    company_contacts = Company.find(params[:company_id]).contacts.send(params[:status])
    #company_clients

    # get the unique clients associated with companies and accounts
    contacts = (company.contacts.send(params[:status]) + company_contacts).uniq

    # sort clients in ascending or descending order
    contacts.sort! do |a, b|
      b, a = a, b if params[:sort_direction] == 'desc'
      params[:sort_column] = 'contact_name' if params[:sort_column].starts_with?('concat')
      a.send(params[:sort_column]) <=> b.send(params[:sort_column])
    end if params[:sort_column] && params[:sort_direction]

    Kaminari.paginate_array(contacts).page(params[:page]).per(params[:per])

  end


end
