class AddBankInfoToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :bank_info, :string
  end
end
