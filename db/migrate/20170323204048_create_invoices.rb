class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.string :invoice_number
      t.datetime :invoice_date
      t.string :terms
      t.string :notes
      t.string :status
      t.decimal :invoice_total, precision: 10, scale: 2

      t.timestamps
    end
  end
end
