class AddArchiveNumberToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :archive_number, :string
  end
end
