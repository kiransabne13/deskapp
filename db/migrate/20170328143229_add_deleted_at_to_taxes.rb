class AddDeletedAtToTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :taxes, :deleted_at, :datetime
    add_index :taxes, :deleted_at
  end
end
