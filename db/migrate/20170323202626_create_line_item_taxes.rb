class CreateLineItemTaxes < ActiveRecord::Migration[5.0]
  def change
    create_table :line_item_taxes do |t|
      t.integer :invoice_line_item_id
      t.decimal :percentage, precision: 10, scale: 2
      t.string :name
      t.integer :tax_id

      t.timestamps
    end
  end
end
