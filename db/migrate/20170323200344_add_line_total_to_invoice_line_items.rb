class AddLineTotalToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :line_total, :decimal, precision: 10, scale: 2
  end
end
