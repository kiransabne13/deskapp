class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_uniqueness_of :email, :uniqueness => :true

  has_many :companies
  has_many :contacts
  has_many :items
  has_many :taxes
  has_many :invoices
  has_many :expenses

  def current_company
    companies.first
  end

end
