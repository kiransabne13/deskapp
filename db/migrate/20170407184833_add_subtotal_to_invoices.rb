class AddSubtotalToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :subtotal, :decimal, precision: 10, scale: 2
  end
end
