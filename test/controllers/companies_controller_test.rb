require 'test_helper'

class CompaniesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company = companies(:one)
  end

  test "should get index" do
    get companies_url
    assert_response :success
  end

  test "should get new" do
    get new_company_url
    assert_response :success
  end

  test "should create company" do
    assert_difference('Company.count') do
      post companies_url, params: { company: { company_cin: @company.company_cin, company_city: @company.company_city, company_contact_name: @company.company_contact_name, company_country: @company.company_country, company_email: @company.company_email, company_name: @company.company_name, company_phone_number: @company.company_phone_number, company_state: @company.company_state, company_street_address_1: @company.company_street_address_1, company_street_address_2: @company.company_street_address_2, company_vat: @company.company_vat, company_zipcode: @company.company_zipcode, contact_title: @company.contact_title } }
    end

    assert_redirected_to company_url(Company.last)
  end

  test "should show company" do
    get company_url(@company)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_url(@company)
    assert_response :success
  end

  test "should update company" do
    patch company_url(@company), params: { company: { company_cin: @company.company_cin, company_city: @company.company_city, company_contact_name: @company.company_contact_name, company_country: @company.company_country, company_email: @company.company_email, company_name: @company.company_name, company_phone_number: @company.company_phone_number, company_state: @company.company_state, company_street_address_1: @company.company_street_address_1, company_street_address_2: @company.company_street_address_2, company_vat: @company.company_vat, company_zipcode: @company.company_zipcode, contact_title: @company.contact_title } }
    assert_redirected_to company_url(@company)
  end

  test "should destroy company" do
    assert_difference('Company.count', -1) do
      delete company_url(@company)
    end

    assert_redirected_to companies_url
  end
end
