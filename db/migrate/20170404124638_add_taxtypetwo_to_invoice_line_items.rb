class AddTaxtypetwoToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :taxtypetwo, :string
  end
end
