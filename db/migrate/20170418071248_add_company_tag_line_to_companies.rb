class AddCompanyTagLineToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :company_tag_line, :string
  end
end
