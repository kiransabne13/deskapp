class AddArchiveNumberToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :archive_number, :string
  end
end
