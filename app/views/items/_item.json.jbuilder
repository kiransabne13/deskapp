json.extract! item, :id, :item_name, :item_description, :item_unit_cost, :item_quantity, :tax_1, :tax_2, :item_actual_price, :created_at, :updated_at
json.url item_url(item, format: :json)
