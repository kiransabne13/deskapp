json.extract! invoice_line_item, :id, :item_name, :item_description, :item_unit_cost, :itam_quantity, :gross_line_total, :discount_amount, :tax_1, :tax_2, :created_at, :updated_at
json.url invoice_line_item_url(invoice_line_item, format: :json)
