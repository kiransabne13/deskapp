class AddArchivedAtToContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :archived_at, :datetime
  end
end
