class AddCompanyIdToExpenses < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :company_id, :integer
  end
end
