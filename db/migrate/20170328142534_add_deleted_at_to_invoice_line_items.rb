class AddDeletedAtToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :deleted_at, :datetime
    add_index :invoice_line_items, :deleted_at
  end
end
