class AddTaxamountwoToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :taxamounttwo, :decimal
  end
end
