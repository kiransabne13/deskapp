require 'test_helper'

class InvoiceLineItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice_line_item = invoice_line_items(:one)
  end

  test "should get index" do
    get invoice_line_items_url
    assert_response :success
  end

  test "should get new" do
    get new_invoice_line_item_url
    assert_response :success
  end

  test "should create invoice_line_item" do
    assert_difference('InvoiceLineItem.count') do
      post invoice_line_items_url, params: { invoice_line_item: { discount_amount: @invoice_line_item.discount_amount, gross_line_total: @invoice_line_item.gross_line_total, itam_quantity: @invoice_line_item.itam_quantity, item_description: @invoice_line_item.item_description, item_name: @invoice_line_item.item_name, item_unit_cost: @invoice_line_item.item_unit_cost, tax_1: @invoice_line_item.tax_1, tax_2: @invoice_line_item.tax_2 } }
    end

    assert_redirected_to invoice_line_item_url(InvoiceLineItem.last)
  end

  test "should show invoice_line_item" do
    get invoice_line_item_url(@invoice_line_item)
    assert_response :success
  end

  test "should get edit" do
    get edit_invoice_line_item_url(@invoice_line_item)
    assert_response :success
  end

  test "should update invoice_line_item" do
    patch invoice_line_item_url(@invoice_line_item), params: { invoice_line_item: { discount_amount: @invoice_line_item.discount_amount, gross_line_total: @invoice_line_item.gross_line_total, itam_quantity: @invoice_line_item.itam_quantity, item_description: @invoice_line_item.item_description, item_name: @invoice_line_item.item_name, item_unit_cost: @invoice_line_item.item_unit_cost, tax_1: @invoice_line_item.tax_1, tax_2: @invoice_line_item.tax_2 } }
    assert_redirected_to invoice_line_item_url(@invoice_line_item)
  end

  test "should destroy invoice_line_item" do
    assert_difference('InvoiceLineItem.count', -1) do
      delete invoice_line_item_url(@invoice_line_item)
    end

    assert_redirected_to invoice_line_items_url
  end
end
