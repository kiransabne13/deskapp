json.extract! company, :id, :company_name, :company_contact_name, :contact_title, :company_country, :company_city, :company_street_address_1, :company_street_address_2, :company_state, :company_zipcode, :company_phone_number, :company_email, :company_cin, :company_vat, :created_at, :updated_at
json.url company_url(company, format: :json)
