class AddTaxratetwoToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :taxratetwo, :decimal
  end
end
