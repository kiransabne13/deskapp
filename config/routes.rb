Rails.application.routes.draw do

  resources :expense_categories

  resources :expenses do
    resources :expense_categories do
    end
  end



  resources :invoices do
    resources :invoice_line_items 
  end
    



  resources :invoice_line_items
  resources :taxes

  resources :items do
    collection do
      post 'load_item_data'
      get 'duplicate_item'
    end
  end

  resources :contacts do
  end


  devise_for :users

  resources :companies do
    collection do
      post 'get_contacts_and_items'
    end
  end

  root 'welcome#index'


  devise_scope :user do
        get '/users/sign_out' => 'devise/sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    get ':controller(/:action(/:id))(.:format)'

end
end
