# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170424054110) do

  create_table "companies", force: :cascade do |t|
    t.string   "company_name"
    t.string   "company_contact_name"
    t.string   "contact_title"
    t.string   "company_country"
    t.string   "company_city"
    t.string   "company_street_address_1"
    t.string   "company_street_address_2"
    t.string   "company_state"
    t.string   "company_zipcode"
    t.string   "company_phone_number"
    t.string   "company_email"
    t.string   "company_cin"
    t.string   "company_vat"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.string   "bank_info"
    t.string   "companylogo"
    t.string   "company_tag_line"
    t.string   "companywebsite"
    t.index ["deleted_at"], name: "index_companies_on_deleted_at"
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "contact_company_name"
    t.string   "contact_first_name"
    t.string   "contact_last_name"
    t.string   "contact_email"
    t.string   "contact_company_phone"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "company_id"
    t.integer  "user_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.integer  "expense_id"
    t.index ["deleted_at"], name: "index_contacts_on_deleted_at"
  end

  create_table "expense_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "expense_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.decimal  "expense_amount"
    t.datetime "expense_date"
    t.string   "expense_note"
    t.string   "taxtypeone"
    t.decimal  "taxrateone"
    t.decimal  "taxamountone"
    t.string   "taxtypetwo"
    t.decimal  "taxratetwo"
    t.decimal  "taxamounttwo"
    t.decimal  "expense_total"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "contact_id"
    t.integer  "expense_category_id"
  end

  create_table "invoice_line_items", force: :cascade do |t|
    t.string   "item_name"
    t.string   "item_description"
    t.decimal  "item_unit_cost",   precision: 10, scale: 2
    t.decimal  "itam_quantity",    precision: 10, scale: 2
    t.decimal  "gross_line_total", precision: 10, scale: 2
    t.decimal  "discount_amount",  precision: 10, scale: 2
    t.integer  "tax_1"
    t.integer  "tax_2"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "item_id"
    t.decimal  "line_total",       precision: 10, scale: 2
    t.integer  "invoice_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.decimal  "item_quantity",    precision: 10, scale: 2
    t.decimal  "taxamountone"
    t.string   "taxtypeone"
    t.decimal  "taxrateone",       precision: 10, scale: 2
    t.decimal  "taxamounttwo"
    t.decimal  "taxratetwo"
    t.string   "taxtypetwo"
    t.decimal  "net_total",        precision: 10, scale: 2
    t.decimal  "subtotal",         precision: 10, scale: 2
    t.decimal  "shippingcharge",   precision: 10, scale: 2
    t.decimal  "invoicetotal",     precision: 10, scale: 2
    t.index ["deleted_at"], name: "index_invoice_line_items_on_deleted_at"
  end

  create_table "invoices", force: :cascade do |t|
    t.string   "invoice_number"
    t.datetime "invoice_date"
    t.string   "terms"
    t.string   "notes"
    t.string   "status"
    t.decimal  "invoice_total",   precision: 10, scale: 2
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "contact_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.decimal  "subtotal",        precision: 10, scale: 2
    t.decimal  "shippingcharges", precision: 10, scale: 2
    t.datetime "due_date"
    t.string   "invoice_word"
    t.index ["deleted_at"], name: "index_invoices_on_deleted_at"
  end

  create_table "items", force: :cascade do |t|
    t.string   "item_name"
    t.string   "item_description"
    t.decimal  "item_unit_cost",    precision: 10, scale: 2
    t.decimal  "item_quantity",     precision: 10, scale: 2
    t.integer  "tax_1"
    t.integer  "tax_2"
    t.decimal  "item_actual_price", precision: 10, scale: 2
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
  end

  create_table "line_item_taxes", force: :cascade do |t|
    t.integer  "invoice_line_item_id"
    t.decimal  "percentage",           precision: 10, scale: 2
    t.string   "name"
    t.integer  "tax_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_line_item_taxes_on_deleted_at"
  end

  create_table "taxes", force: :cascade do |t|
    t.string   "name"
    t.decimal  "percentage"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "archive_number"
    t.datetime "archived_at"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_taxes_on_deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
