class AddExpenseCategoryIdToExpenses < ActiveRecord::Migration[5.0]
  def change
    add_column :expenses, :expense_category_id, :integer
  end
end
