class AddTaxamountoneToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :taxamountone, :decimal
  end
end
