class Expense < ApplicationRecord

  belongs_to :contact, optional: true
  belongs_to :company, optional: true
  belongs_to :user, optional: true
  belongs_to :expense_category, optional: true
  
end
