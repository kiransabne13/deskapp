class AddArchivedAtToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :archived_at, :datetime
  end
end
