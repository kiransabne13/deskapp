class CompanyMailer < ApplicationMailer
  default from: "kiransabne18@gmail.com"
  
  def added_company(company)
    @company = company

    mail to: company.company_email, subject: "New Company Created" 
  end
end
