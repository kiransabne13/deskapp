class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.decimal :expense_amount
      t.datetime :expense_date
      t.string :expense_note
      t.string :taxtypeone
      t.decimal :taxrateone
      t.decimal :taxamountone
      t.string :taxtypetwo
      t.decimal :taxratetwo
      t.decimal :taxamounttwo
      t.decimal :expense_total

      t.timestamps
    end
  end
end
