class Item < ApplicationRecord

  scope :multiple, lambda { |ids| where('id IN(?)', ids.is_a?(String) ? ids.split(',') : [*ids]) }
  scope :archive_multiple, lambda { |ids| multiple(ids).map(&:archive) }
  scope :delete_multiple, lambda { |ids| multiple(ids).map(&:destroy) }

  has_many :invoice_line_items
  belongs_to :company, optional: true
  belongs_to :user, optional: true
  belongs_to :tax1, :foreign_key => "tax_1", :class_name => "Tax", optional: true
  belongs_to :tax2, :foreign_key => "tax_2", :class_name => "Tax", optional: true

  acts_as_archival
  acts_as_paranoid

  def self.is_exists? item_name, company_id = nil
    company = Company.find company_id if company_id.present?
    company.present? ? company.items.where(:item_name => item_name).present? : where(:item_name => item_name).present?
  end

  def self.recover_archived(ids)
    multiple(ids).map(&:unarchive)
  end

  def self.recover_deleted(ids)
    multiple(ids).only_deleted.each { |item| item.restore; item.unarchive }
  end

  def self.filter(params, per_page)
    mappings = {active: 'unarchived', archived: 'archived', deleted: 'only_deleted'}
    method = mappings[params[:status].to_sym]
    self.send(method).page(params[:page]).per(per_page)
  end

  def self.get_items(params)
    company = params[:user].current_company

    # get the items associated with companies
    company_id = params['current_company'] || params[:user].current_company
    company_items = Company.find(company_id).items.send(params[:status])

    # get the unique items associated with companies and accounts
    items = (company.items.send(params[:status]) + company_items).uniq

    # sort items in ascending or descending order
    items.sort! do |a, b|
      b, a = a, b if params[:sort_direction] == 'desc'

      if %w(tax1.name tax2.name).include?(params[:sort_column])
        item1 = a.send(params[:sort_column].split('.')[0]).send(params[:sort_column].split('.')[1]) rescue ''
        item2 = b.send(params[:sort_column].split('.')[0]).send(params[:sort_column].split('.')[1]) rescue ''

        #TODO change the above logic to eval
        #item1 = eval("a.#{params[:sort_column]}") rescue ''
        #item2 = eval("b.#{params[:sort_column]}") rescue ''
        item1 <=> item2
      elsif a.send(params[:sort_column]).class.to_s == 'BigDecimal' and b.send(params[:sort_column]).class.to_s == 'BigDecimal'
        a.send(params[:sort_column]).to_i <=> b.send(params[:sort_column]).to_i
      else
        a.send(params[:sort_column]).to_s <=> b.send(params[:sort_column]).to_s
      end
    end if params[:sort_column] && params[:sort_direction]

    Kaminari.paginate_array(items).page(params[:page]).per(params[:per])

  end

end
