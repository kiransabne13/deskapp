class ItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  # GET /items
  # GET /items.json
  def index
    @items = current_user.current_company.items
  end
  # GET /items/1
  # GET /items/1.json
  def show
    @items = current_user.current_company.items.find(params[:id])
  end

  # GET /items/new
  def new
    @item = current_user.current_company.items.new
  end

  # GET /items/1/edit
  def edit
    @item = current_user.current_company.items.find(params[:id])
  end

  # POST /items
  # POST /items.json
  def create
    @item = current_user.current_company.items.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    @item = current_user.current_company.items.find(params[:id])
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item = current_user.current_company.items.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def load_item_data
    item = Item.find_by_id(params[:id]).present? ?  Item.find(params[:id]) : Item.unscoped.find_by_id(params[:id])
    render :text => [item.item_description || "", item.item_unit_cost.to_f || 1, item.item_quantity.to_i || 1, item.tax_1 || 0, item.tax_2 || 0, item.item_name || ""]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:item_name, :item_description, :item_unit_cost, :item_quantity, :tax_1, :tax_2, :item_actual_price)
    end
end
