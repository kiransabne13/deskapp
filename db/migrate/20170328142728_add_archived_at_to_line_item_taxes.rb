class AddArchivedAtToLineItemTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :line_item_taxes, :archived_at, :datetime
  end
end
