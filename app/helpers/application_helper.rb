module ApplicationHelper



  def filter_by_companies
    companies = current_user.companies
    content_tag(:ul) do
      companies.each do |company|
        params[:company_id] = company.id
        if params[:controller] == "dashboard"
          url_param = "javascript:"
          remote_status = false
        else
          url_param = url_for(params: params)
          remote_status = true
        end
        link_options = {:remote => remote_status, :class => 'header_company_link', :company_id => company.id, :controller => params[:controller], :action => params[:action]}
        concat(content_tag(:li) { link_to(company.company_name, url_param, link_options) })
      end
    end
  end

  def query_string(params)
    "&page=#{params[:page]}&per=#{params[:per]}&company_id=#{params[:company_id]}&sort=#{params[:sort]}&direction=#{params[:direction]}"
  end


  def get_company_name
    company_id = session['current_company'] || current_user.current_company || current_user.first_company_id
    Company.find(company_id).company_name
  end

  def get_user_current_company
    company_id = session['current_company'] || current_user.current_company || current_user.first_company_id
    Company.find(company_id)
  end

  def get_invoice_company_name(invoice=nil)
    company = invoice.company
    if company.present?
      company.company_name
    else
      company_id = session['current_company'] || current_user.current_company || current_user.first_company_id
      Company.find(company_id).company_name
    end
  end


end
