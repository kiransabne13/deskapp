class AddInvoiceIdToInvoiceLineItems < ActiveRecord::Migration[5.0]
  def change
    add_column :invoice_line_items, :invoice_id, :integer
  end
end
