class AddDeletedAtToLineItemTaxes < ActiveRecord::Migration[5.0]
  def change
    add_column :line_item_taxes, :deleted_at, :datetime
    add_index :line_item_taxes, :deleted_at
  end
end
