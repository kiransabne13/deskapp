class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :contact_company_name
      t.string :contact_first_name
      t.string :contact_last_name
      t.string :contact_email
      t.string :contact_company_phone

      t.timestamps
    end
  end
end
