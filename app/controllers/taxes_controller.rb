class TaxesController < ApplicationController
  before_action :authenticate_user!
 # before_action :set_tax, only: [:show, :edit, :update, :destroy]

  # GET /taxes
  # GET /taxes.json
  def index
    @taxes = current_user.current_company.taxes
  end

  # GET /taxes/1
  # GET /taxes/1.json
  def show
    @taxis = current_user.current_company.taxes.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @taxis }
    end
  end

  # GET /taxes/new
  def new
    @taxis = current_user.current_company.taxes.new
  end

  # GET /taxes/1/edit
  def edit
    @taxis = current_user.current_company.taxes.find(params[:id])
  end

  # POST /taxes
  # POST /taxes.json
  def create
    @taxis = current_user.current_company.taxes.new(taxes_params)

    respond_to do |format|
      if @taxis.save
        format.html { redirect_to @taxis, notice: 'Tax was successfully created.' }
        format.json { render :show, status: :created, location: @taxis }
      else
        format.html { render :new }
        format.json { render json: @taxis.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /taxes/1
  # PATCH/PUT /taxes/1.json
  def update
    @taxis = current_user.current_company.taxes.find(params[:id])
    respond_to do |format|
      if @taxis.update(taxes_params)
        format.html { redirect_to @taxes, notice: 'Tax was successfully updated.' }
        format.json { render :show, status: :ok, location: @tax }
      else
        format.html { render :edit }
        format.json { render json: @taxis.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /taxes/1
  # DELETE /taxes/1.json
  def destroy
    @taxis = current_user.current_company.taxes.find(params[:id])
    @taxis.destroy
    respond_to do |format|
      format.html { redirect_to taxes_url, notice: 'Tax was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
#    def set_tax
 #     @tax = Tax.find(params[:id])
  #  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def taxes_params
      params.require(:tax).permit(:name, :percentage)
    end
end
