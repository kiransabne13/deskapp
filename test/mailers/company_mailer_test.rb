require 'test_helper'

class CompanyMailerTest < ActionMailer::TestCase
  test "added_company" do
    mail = CompanyMailer.added_company
    assert_equal "Added company", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
